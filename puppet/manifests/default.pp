# Timezone
##########################################################################
# COMPTOIR-DU-LIBRE software configures the time zone
# used in web application (by default, it is Europe/Paris),
# independently of the time zone used by the operating system.

# Operating system use Europe/Paris timezone
exec { 'timedatectl set-timezone':
    command => "/usr/bin/timedatectl set-timezone Europe/Paris",
    unless  => "/usr/bin/timedatectl | grep Europe/Paris",
}
    ##########################################################################
    #     # Operating system use America/New_York timezone
    #     exec { 'timedatectl set-timezone':
    #         command => "/usr/bin/timedatectl set-timezone America/New_York",
    #         unless  => "/usr/bin/timedatectl | grep America/New_York",
    #     }
    ##########################################################################

# Prerequired Puppet component modules
##########################################################################
include ::apt
include mailhog
# include apache
# include comptoir

# Comptoir-du-Libre prerequired packages
##########################################################################
$preRequistes = [
    'php8.1-bcmath',
    'php8.1-curl',
    'php8.1-pgsql',
    'php8.1-mbstring',
    'php8.1-intl',
    'php8.1-xml',
]
package { $preRequistes:
    ensure  => 'installed',
    require => Class['apt::update'],
}

# Comptoir-du-Libre software
##########################################################################
class { 'comptoir':
    require => [
        Class['Postgresql::Server'],
        Class['apache'],
        Class['apache::mod::php'],
    ],
}

# Disable HTTPS Comptoir-du-Libre configuration
file { '/opt/comptoir/comptoir/config/packages/prod/':
    ensure => 'directory',
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0750',
    require => [
        Class['comptoir'],
    ],
}
file { '/opt/comptoir/comptoir/config/packages/prod/nelmio_security.yaml':
    ensure => 'file',
    mode   => '0640',
    content => "nelmio_security:\n    forced_ssl:\n        enabled: false",
    require => [
        Class['comptoir'],
        File["/opt/comptoir/comptoir/config/packages/prod/"],
    ],
}
exec { 'console cache:warmup, after disable HTTPS Comptoir-du-Libre configuration':
    command     => "/opt/comptoir/comptoir/bin/console cache:clear",
    cwd         => "/opt/comptoir/comptoir",
    user        => 'www-data',
    group       => 'www-data',
    environment => [
        'APP_ENV=prod',
        'APP_DEBUG=0',
    ],
    require => [
        Class['comptoir'],
        File["/opt/comptoir/comptoir/config/packages/prod/nelmio_security.yaml"],
    ],
}



# Enabled Comptoir-du-Libre DEBUG_CERT_BY_ADMIN env
#     exec { 'comptoir DEBUG_CERT_BY_ADMIN  enabled':
#         command => "/bin/echo 'DEBUG_CERT_BY_ADMIN=enabled' >>  /opt/comptoir/comptoir/.env.local",
#         unless  => "/bin/cat /opt/comptoir/comptoir/.env.local | grep 'DEBUG_CERT_BY_ADMIN=enabled'",
#         require => [
#             Class['comptoir'],
#         ],
#     }

# Configure Apache server and vhost
##########################################################################
class { 'apache':
    default_vhost    => false,
    mpm_module       => 'prefork',
    default_mods    => [
        'mime',     # cfssl crl
        'php',      # comptoir
        'rewrite',  # comptoir
        'headers'   # comptoir (required to add cache headers and security headers)
    ]
}

apache::vhost { 'comptoir.example.com':
    port    => 80,
    docroot => '/opt/comptoir/comptoir/public',
    docroot_owner => 'www-data',
    docroot_group => 'www-data',
    directories   => [
        {
            path           => '/opt/comptoir/comptoir/public',
            allow_override => 'All',
        },
    ],
}
