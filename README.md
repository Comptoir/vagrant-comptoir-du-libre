[![Last release](https://gitlab.adullact.net/Comptoir/vagrant-comptoir-du-libre/-/badges/release.svg)](https://gitlab.adullact.net/Comptoir/vagrant-comptoir-du-libre/-/releases)
[![Pipelines](https://gitlab.adullact.net/Comptoir/vagrant-comptoir-du-libre/badges/main/pipeline.svg)](https://gitlab.adullact.net/Comptoir/vagrant-comptoir-du-libre/pipelines?ref=main)
[![License : AGPL v3](https://img.shields.io/badge/License-AGPL3-blue.svg)](LICENSE.txt)
[![Code of Conduct](https://img.shields.io/badge/Contributor-Code%20of%20conduct-blueviolet.svg?style=flat-square)](CODE_OF_CONDUCT.md)
[![Contributing Welcome](https://img.shields.io/badge/Contributing-Welcome-fc7700.svg?style=flat-square)](CONTRIBUTING.md)

# Vagrant Comptoir-du-Libre

[[_TOC_]]

## About

This project lets you build from scratch and in few minutes
a running [**Comptoir-du-Libre** webapp](https://gitlab.adullact.net/Comptoir/comptoir-du-libre)
in a virtual machine on your laptop.

- [Changelog](CHANGELOG.md)
- [Contributing](CONTRIBUTING.md)
- [Code of Conduct](CODE_OF_CONDUCT.md)

### License

[**AGPL v3** - GNU Affero General Public License](LICENSE)

### Warning ⚠️ : only for test or demonstration purposes

> - Created **only** for **test** or **demonstration** purposes.
> - **No security** has been made for production.

## Technical details

### HTTP ports, URLs and credentials

| Service             | Port | URL                     | User                            | Password                                           |
|---------------------|------|-------------------------|---------------------------------|----------------------------------------------------|
| **Webmail** MailHog | 8025 | `http://127.0.0.1:8025` |                                 |                                                    |
| **Comptoir** webapp | 8002 | `http://127.0.0.1:8002` | `superadmin_webapp@example.org` | `superadmin-webapp_PaSsWord_IsNotSoSecretChangeIt` |

### Databases

| PostgreSQL            | Port | Database               | User               | Password                        |
|-----------------------|------|------------------------|--------------------|---------------------------------|
| **Comptoir** database | 5432 | `comptoir_pg_database` | `comptoir_pg_user` | `comptoir_pg_password_ChangeIt` |

Access to **Comptoir** database from your IDE:
> `postgresql://comptoir_pg_user:comptoir_pg_password_ChangeIt@localhost:5432/comptoir_pg_database?serverVersion=14&charset=utf8`

### Shared folders: Host <--> Guest (VM)

| Comptoir webapp | Host directories            | Guest (VM) directories   |
|-----------------|-----------------------------|--------------------------|
| Source code     | `./VM_shared/webapp`        | `/opt/comptoir/`         |
| Configuration   | `/VM_shared/webapp_config`  | `/etc/comptoir/`         |
| Logs            | `/VM_shared/webapp_log`     | `/var/comptoir/log/`     |
| Sessions        | `/VM_shared/webapp_session` | `/var/comptoir/sessions` |


## Documentation

### Prerequisites

On your computer you need to have installed:

- [Vagrant](https://www.vagrantup.com/downloads)
- [VirtualBox](https://www.virtualbox.org/)
- Git
- a Ruby version management tool like RVM or [RBenv](https://github.com/rbenv/rbenv)
- r10k (`gem install r10k`)
- Internet access
- Bash

For Linux users (debian, ubuntu), do *not* install Vagrant and VirtualBox from `apt-get` (packages are way too old),
download `.deb` from the respective websites.

---------------------------------

### Setup

Three steps to get you own running Comptoir-du-Libre on your laptop.

#### Step 1 - Clone this repository

```bash
git clone https://gitlab.adullact.net/Comptoir/vagrant-comptoir-du-libre.git
cd  vagrant-comptoir-du-libre
```

#### Step 2 - Download all required Puppet modules used to configure the virtual machine

```bash
# To be executed in the directory containing the Vagrantfile

# Download all required Puppet modules used to configure the virtual machine
./BUILD.sh
```

#### Step 3 - Boot a virtual machine, download and configure all required items to get a running CFSSL

Some **Comptoir-du-Libre** configuration values
are presents in [`puppet/hieradata/parameters.yaml`](puppet/hieradata/parameters.yaml) file.

> You can modify this file to change value of any parameter documented by:
>
> - [**Puppet** component module **Comptoir-du-Libre**](https://gitlab.adullact.net/Comptoir/puppet-comptoir-du-libre/-/blob/main/REFERENCE.md)

```bash
# To be executed in the directory containing the Vagrantfile

# Creates and starts the VM (Virtual Machine) according to the Vagrantfile
vagrant destroy -f  # stops the running machine Vagrant and destroys all resources
vagrant up          # Then you wait few minutes
                    # depends on your network access and power of your computer

# Stops gracefully the VM
vagrant halt

# Restart the VM
vagrant up

# Stops the VM and destroys all resources
# that were created during the machine creation process.
vagrant destroy -f
```

#### How to start the VM with a custom Comptoir-du-Libre port?

```bash
# Creates and starts the VM (Virtual Machine)
# with some customizations (ports, ...)
# - customize Comptoir   port  ---> 8002      (default, port allowed above 1000)
# - customize MailHog    port  ---> 5432      (default, port allowed above 1000)
# - customize PostgreSQL port  ---> 5432      (default, port allowed above 1000)
VAGRANT_HOST_COMPTOIR_PORT=8080        \
VAGRANT_HOST_MAILHOG_HTTP_PORT=8025  \
VAGRANT_HOST_POSTGRESQL_PORT=5439    \
vagrant up
```


---------------------------------

### Comptoir-du-Libre - Usage documentation

Use of Comptoir-du-Libre software:

- In your browser, go to `http://127.0.0.1:8002/`
- Use this following user and password to login:
  - `superadmin_webapp@example.org`
  - `superadmin-webapp_PaSsWord_IsNotSoSecretChangeIt`

Browsing e-mails sent by Comptoir-du-Libre software in webmail:

- In your browser, go to `http://127.0.0.1:8025/`


Some useful command lines:

```bash
# Display application logs
vagrant ssh -c 'sudo -i tail -f /var/comptoir/log/prod-$(date '+%Y-%m-%d').log'

# Lists all dotenv files with variables and values
vagrant ssh -c 'sudo -i /opt/comptoir/comptoir/bin/console debug:dotenv'

# Lists all application routes (URL, HTTP method, ...)
vagrant ssh -c 'sudo -i /opt/comptoir/comptoir/bin/console debug:router'
```
