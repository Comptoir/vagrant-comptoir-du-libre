# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

--------------------------------

## v1.2.0, 2024.09.10

### Added

- feat(make): add `clean_all` command

### Fixed

- documentation: update webapp account

### Changed

- feat: use Comptoir webapp **3.0.0.alpha.9**
- fix: use the new puppet module configuration (see: `adullact/puppet-comptoir-du-libre` **0.2.0**)

### Dependancies

- use `adullact/puppet-comptoir-du-libre` **0.2.0** instead of 0.1.0


--------------------------------

## v1.1.0, 2024.06.18

### Added

- feat: add Makefile

### Changed

- feat: using Comptoir webapp **3.0.0.alpha.6**

### Dependancies

- use `puppetlabs-apache` **12.1.0** instead of 9.1.2
- use `puppetlabs-apt` **9.4.0** instead of 8.5.0
- use `puppetlabs-concat` **9.0.2** instead of 7.3.1
- use `puppetlabs-postgresql` **10.3.0** instead of 8.2.1
- use `puppetlabs-stdlib` **9.6.0** instead of 8.5.0
- use `puppetlabs-vcsrepo` **5.0.0** instead of 6.1.0
- use `voxpupuli/puppet-archive` **7.1.0** instead of 6.0.2
- use `voxpupuli/puppet-systemd` **v7.1.0** instead of 4.2.0
- use `adullact/puppet-comptoir-du-libre` **0.1.0** instead of 0.1.0-alpha2


--------------------------------

## v1.0.0, 2024.06.07

### Changed

- feat: using Comptoir webapp **3.0.0.alpha.2**


--------------------------------

## v0.1.0, 2024.06.06

### Added

- feat: using Puppet module **0.1.0-alpha1**
- feat: using Comptoir webapp **3.0.0.alpha.1**


--------------------------------

## Template

```markdown
## <major>.<minor>.patch_DEV     (unreleased)

### Added

### Changed

#### Breaking change

### Fixed

### Dependancies

### Security

--------------------------------

```
