$script_ubuntu = <<-SCRIPT
if [ ! -f /opt/puppetlabs/bin/puppet ]; then
  sudo wget --quiet https://apt.puppetlabs.com/puppet7-release-jammy.deb # Ubuntu 22.04
  sudo dpkg -i puppet7-release-jammy.deb                                 # Ubuntu 22.04
  sudo apt-get update
  sudo apt-get install puppet-agent
fi
SCRIPT

Vagrant.configure("2") do |config|

  # Default values
  default_host_postgresql_port = 5432
  default_host_mailhog_port    = 8025
  default_host_comptoir_port   = 8002
  default_host_comptoir_ip     = "127.0.0.1"

  # Environment variable customizations
  host_postgresql_port = ENV['VAGRANT_HOST_POSTGRESQL_PORT']   ? ENV['VAGRANT_HOST_POSTGRESQL_PORT']   : default_host_postgresql_port
  host_mailhog_port    = ENV['VAGRANT_HOST_MAILHOG_HTTP_PORT'] ? ENV['VAGRANT_HOST_MAILHOG_HTTP_PORT'] : default_host_mailhog_port
  host_comptoir_port   = ENV['VAGRANT_HOST_COMPTOIR_PORT']     ? ENV['VAGRANT_HOST_COMPTOIR_PORT']     : default_host_comptoir_port
  host_comptoir_ip     = ENV['VAGRANT_HOST_COMPTOIR_IP']       ? ENV['VAGRANT_HOST_COMPTOIR_IP']       : default_host_comptoir_ip

  config.vm.box = "ubuntu/jammy64" # Ubuntu 22.04
  config.vm.hostname = "comptoir.example.org"
  config.vm.network "forwarded_port", id: 'Comptoir',   guest: 80,   host: host_comptoir_port,   auto_correct: true, host_ip: host_comptoir_ip
  config.vm.network "forwarded_port", id: 'MailHog',    guest: 8025, host: host_mailhog_port,    auto_correct: true, host_ip: host_comptoir_ip
  config.vm.network "forwarded_port", id: 'PostgreSQL', guest: 5432, host: host_postgresql_port, auto_correct: true, host_ip: host_comptoir_ip
  config.vm.provider "virtualbox" do |vb|
    vb.name = "DEMO_COMPTOIR"
#   vb.memory = "4096"
#   vb.cpus = "4"
  end

  config.vm.synced_folder "puppet/hieradata/", "/tmp/vagrant-puppet/hieradata"
  config.vm.provision "shell", inline: $script_ubuntu
  config.vm.provision "puppet" do |puppet|
    # documentation
    # https://developer.hashicorp.com/vagrant/docs/provisioning/puppet_apply
    puppet.hiera_config_path = "puppet/hiera.yaml"
    puppet.working_directory = "/tmp/vagrant-puppet"
    puppet.module_path = "puppet/modules"
    puppet.manifest_file = "default.pp"
    puppet.manifests_path= "puppet/manifests"
#   puppet.options = "--debug"
  end

  # Shared folders: Host <--> Guest (VM)
  ###############################################################################################
  ###############################################################################################

  # Default share folder:  Host  "./"  <--> Guest (VM) "/vagrant"
  #
  # Share an additional folder to the guest VM.
  # - The first argument is the path on the host to the actual folder.
  # - The second argument is the path on the guest to mount the folder.
  # - And the optional third argument is a set of non-required options.
  config.vm.synced_folder "./VM_shared/webapp",         "/opt/comptoir/",         create: true, owner: "www-data", group: "www-data"
  config.vm.synced_folder "./VM_shared/webapp_config",  "/etc/comptoir/",         create: true, owner: "www-data", group: "www-data"
  config.vm.synced_folder "./VM_shared/webapp_log",     "/var/comptoir/log/",     create: true, owner: "www-data", group: "www-data"
  config.vm.synced_folder "./VM_shared/webapp_session", "/var/comptoir/sessions", create: true, owner: "www-data", group: "www-data"

  # Message to show after vagrant up
  config.vm.post_up_message = <<-MESSAGE
  --------------------------------------------------------
  ⚠️   Created only for test or demonstration purposes. ⚠️
  --------------------------------------------------------
  ⚠️   No security has been made for production.        ⚠️
  --------------------------------------------------------


  Comptoir-du-Libre software
  ----------------------------------------------------------------------

  # Display application logs
  vagrant ssh -c 'sudo -i tail -f /var/comptoir/log/prod-$(date '+%Y-%m-%d').log'

  # Lists all dotenv files with variables and values
  vagrant ssh -c 'sudo -i /opt/comptoir/comptoir/bin/console debug:dotenv'

  # Lists all application routes (URL, HTTP method, ...)
  vagrant ssh -c 'sudo -i /opt/comptoir/comptoir/bin/console debug:router'

  ----------------------------------------------------------------------
  Use 'vagrant port' command line to see port mapping.

  Default:   5432 (guest) --> #{host_postgresql_port} (host)  PostgreSQL    port
             8025 (guest) --> #{host_mailhog_port} (host)  MailHog  HTTP port
               80 (guest) --> #{host_comptoir_port} (host)  Comptoir HTTP port

  ----------------------------------------------------------------------
  Webmail ............. http://#{host_comptoir_ip}:#{host_mailhog_port}
  Comptoir webapp ..... http://#{host_comptoir_ip}:#{host_comptoir_port}
  ----------------------------------------------------------------------
  Comptoir Account .... superadmin_webapp@example.org
  Comptoir Password ... superadmin-webapp_PaSsWord_IsNotSoSecretChangeIt
  ----------------------------------------------------------------------

  --------------------------------------------------------
  ⚠️   Created only for test or demonstration purposes. ⚠️
  --------------------------------------------------------
  MESSAGE
end
